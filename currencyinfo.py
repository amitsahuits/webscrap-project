import requests
from bs4 import BeautifulSoup
from bs4 import BeautifulSoup
import csv
# from requests.models import Response

res = requests.get('https://coinstats.app/coins/')


htmlcontent = res.content

soup = BeautifulSoup(htmlcontent, 'html.parser')

name_list = []
price_list = []
btc_list = []
cap_list = []
volume_list = []

print('name----------')
products = soup.select('div.coin-with-logo>span')
for product in products:
    name = product.text
    # print(name)
    name_list.append(name)
   

print('price----------')
products = soup.select('span.price')
for product in products:
    price = product.text.strip()
    # print(price)
    price_list.append(price)

print('btc----------')
products = soup.select('td.right>a>span.other-color')

#using slicing method becuase two column in websites having a same class and we are getting those two table data alternatively at even and odd places, so to get data separately.
#at odd place we're btc so here i applied odd condition
for product in products[1:-1:2]:
    btc = product.text.strip()
    # print(btc)
    btc_list.append(btc)

print('cap----------')
products = soup.select('span.cap')
#at even place we're getting Market cap so here i applied even condition
for product in products[0:-1:2]:
    cap = product.text.strip()
    cap_list.append(cap)
    # print(cap)


print('volume-----------')
products = soup.select('span.cap')
for product in products[1:-1:2]:
    volume = product.text.strip()
    volume_list.append(volume)
    # print(volume)

with open("coindetail.csv","w",newline='') as f: 
    w=csv.writer(f) # returns csv writer object 
    w.writerow(["Name","price","btc","market cap","volume"])
    i=0
    while i<99:
        w.writerow([name_list[i],price_list[i],btc_list[i],cap_list[i],volume_list[i]])
        i = i+1
